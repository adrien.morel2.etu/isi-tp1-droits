#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
printf("My real uid is %d\n", getuid());
printf("My real gid is %d\n", getgid());
printf("My effective uid is %d\n", geteuid());
printf("My effective gid is %d\n", getegid());
int c;
FILE *file;
file = fopen(argv[1], "r");
if (file) {
    printf("File content : ")
    while ((c = getc(file)) != EOF)
        putchar(c);
    fclose(file);
}

return 0;
}
