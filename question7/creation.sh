#!bin/bash

#Ajout de l'admin + connection en tant que celui-ci pour créer les directories lui appartenant
useradd admin_g;
sudo su admin_g;

#Création des deux groupes nécessaires
groupadd groupe_a;
groupadd groupe_b;

#Création des deux utlisateurs + ajout à leur groupe respectif
useradd lambda_a -g groupe_a;
useradd lambda_b -g groupe_b;

#Création des directories
mkdir dir_a;
mkdir dir_b;
mkdir dir_c;

exit;

#Attribution des groupes au directory associés a ceux-ci
sudo chgrp groupe_a dir_a;
sudo chgrp groupe_b dir_b;

#Donnes tous les droits a l'admin ainsi qu'au groupe_a à ce drectory et aucuns droits aux autres utilisateurs
sudo chmod 770 dir_a;
#Seuls le propriétaire du répertoire, et le propriétaire d'un fichier qui s'y trouve ont le droit de supprimer ce fichier.
sudo chmod +t dir_a;

#Donnes tous les droits a l'admin ainsi qu'au groupe_b à ce drectory et aucuns droits aux autres utilisateurs
sudo chmod 770 dir_b;
#Seuls le propriétaire du répertoire, et le propriétaire d'un fichier qui s'y trouve ont le droit de supprimer ce fichier.
sudo chmod +t dir_b;

#Donnes tous les droits a l'admin ainsi qu'au groupe (encore non défini) à ce drectory et les droits de lecture et d'exécution aux autres utilisateurs
#(groupe_a et groupe_b)
sudo chmod 775 dir_c;
#Seuls le propriétaire du répertoire, et le propriétaire d'un fichier qui s'y trouve ont le droit de supprimer ce fichier.
sudo chmod +t dir_c;

su lambda_a;
touch dir_a/test_a;
echo "je suis le fichier test_a" >> dir_a/test_a;
exit;

su lambda_b;
touch dir_b/test_b;
echo "je suis le fichier test_b" >> dir_b/test_b;
exit;

su admin;
touch dir_c/test_c;
echo "je suis le fichier test_c" >> dir_c/test_c;
exit;

