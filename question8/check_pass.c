#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "check_pass.h"


int verify_password(char* username, char* mdp){
    FILE* fpasswd;

    fpasswd = fopen("/home/admin/passwd", "r");

    if(fpasswd == NULL){
        printf("Le fichier passwd n'a pas pu être ouvert");
        return 1;
    }

    char * line = NULL;
    size_t len = 0;

    while (getline(&line, &len, fpasswd) != -1) {
        //En partant du principe que le fichier passwd contient les mots de passe sous la forme "username:motdepasse"
        char delim[] = ":";

        //Split une ligne en 2 variables tmp_user => le username et tmp_mdp => le mot de passe
	    char *tmp_user = strtok(line, delim);
        char *tmp_mdp = strtok(NULL, delim);

        if(strcmp(tmp_user,username) == 0){
            if(strcmp(tmp_mdp,mdp) == 0){
                return 0;
            }
            else {
                printf("Mauvais mot de passe");
                return 1;
            }
        }
    }
    printf("L'utilisateur ne se trouve pas dans le fichier passwd");
    return 1;
}
