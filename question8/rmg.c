#include <stdio.h>
#include <stdlib.h>
#include <grp.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <unistd.h>
#include <pwd.h>
#include <string.h>
#include "check_pass.h"


int main(int argc, char const *argv[])
{
    if(argc < 2){
        printf("Pas assez d'arguments");
        return 1;
    }

    struct passwd *pws;
    pws = getpwuid(getuid());
    char* username = pws->pw_name;

    gid_t gid = getgid();

    struct stat fstats;

    struct group * fgroup;

    stat(argv[1], &fstats);

    fgroup = getgrgid(fstats.st_gid);

    if(gid != fgroup->gr_gid){
        printf("Ce fichier n'appartient pas au même groupe que vous");
        return 1;
    }


    FILE * file = fopen(argv[1], "r");

    if(file == NULL){
        printf("Erreur à l'ouverture du fichier");
        exit(1);
    }

    char mdp[256];

    printf("Saisissez votre mot de passe : \n");

    fgets(mdp, sizeof(mdp), stdin);

    if(verify_password(username,mdp) !=0){
        return 1;
    }

    if(remove(argv[1]) != 0){
        printf("Fichier supprimé!");
        return 1;
    }
    
    return 0;
}
