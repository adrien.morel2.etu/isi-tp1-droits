# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Morel Adrien adrien.morel2.etu@univ-lille.fr / Hassan Oualid oualid.hassan3.etu@univ-lille.fr


## Question 1

- Etant donné que toto est dans le groupe ubuntu et que celui-ci a les droits d'écriture sur le fichier titi.txt le programme héritant des droits de 
celui qui l'exécute alors le processus peut en effet écrire.

## Question 2

- Le droit "d'exécuter" un répertoire nous donne en réalité le droit d'accès a son contenu.

- "Permission denied" étant donné que toto fait partie du groupe ubuntu et que celui-ci n'a pas les droit d'accès au contnu du répertoire.

- Seul le nom du fichier peut etre afficher car toto possède encore des droits de lecture sur le fichier.

## Question 3

- RUID : 1001 / RGID : 1000 , EUID : 1001 / EGID : 1000 Non le processus ne peut pas l'ouvrir

- RUID : 1001 / RGID : 1000 , EUID : 1000 / EGID : 1000 Oui le processus arrive a ouvrir le fichier car le processus possède les droits du créateur du fichier 
donc les droits de l'utilisateur ubuntu

## Question 4

- EUID : 1000 / EGID : 1000 Car le script agit comme si il était exécuté par son createur (ubuntu)

## Question 5

La commande chfn permet de changer le nom complet d'un utilisateur ainsi que d'autres informations 
associées a son "compte".

"-rwsr-xr-x 1 root root 85064 Jul 15  2021 /usr/bin/chfn"

L'utilisateur root a tous les droit sur le script, le groupe root peut lire et exécuter le script, 
le reste des utilisateurs ne peuvent que l'exécuter ce qui est logique pour une commande.

## Question 6

Le fichier /etc/passwd ne contient qu'un témoin qu'il existe un mot de passe pour un utilisateur, ce témoin etant 
un 'x' suivant le nom d'utilisateur. Les mots de passe eux sont stockés dans /etc/shadow car on veut que les utilisateurs
aient accès aux informations des autres utilisateurs mais pas aux mots de passes chiffrés de ceux-ci.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








